#########################################################
###                 created by                          ### 
###                Pedro Pasquini                          ###
###        https://inspirehep.net/authors/1467863          ###
###                                                   ### 
###                 creation date                      ### 
###                 20st Aug 2020                      ### 
#########################################################
from pereferences.extractstring import find_first_substring_tag
from pereferences.citation import Citation, peCitationStandards

def AddBibliography(inTag, inCitation, incitation_standard, outcitation_standard):
    if outcitation_standard == 'LaTeX (US)':
        ## this is a temporary fix to keep the current format.
        return {inTag: Citation(inCitation, incitation_standard, 'keep')}
    else:
        return {inTag: Citation(inCitation, incitation_standard, outcitation_standard)}

def ExtractBibliographyInfo(inputFilename: str, incitation_standard: str = 'LaTeX (US)',  outcitation_standard: str = 'neat', startFromFileBeginning = False) -> dict:

    
    if not incitation_standard in 'LaTeX (US)':
        raise NotImplementedError(f"Input {incitation_standard} citation format is not implemented yet!")
    
    if not outcitation_standard in peCitationStandards:
        raise NotImplementedError(f"Output {incitation_standard} citation format is not implemented yet!")

    output_dict  = {}
    current_tag  = ''
    line         = ''
    is_first_bib = True
    beginning_bibliography = startFromFileBeginning

    with open(inputFilename) as fp:
        for file_line in fp:

            ## check if found end string of file.
            has_end_string = find_first_substring_tag(file_line, ini_tag = r"\end{document}", end_tag = -1)
            if not has_end_string[0] == - 1:
                break

            ## check if found end string of bibliography.
            has_end_string = find_first_substring_tag(file_line, ini_tag = r"\end{thebibliography}", end_tag = -1)
            if not has_end_string[0] == - 1 and not startFromFileBeginning:
                if beginning_bibliography: 
                    output_dict = {**output_dict, **AddBibliography(current_tag, line, incitation_standard, outcitation_standard)}
                    current_tag  = ''
                break

            if not beginning_bibliography:
                has_begin_string = find_first_substring_tag(file_line, ini_tag = r"\begin{thebibliography}", end_tag = -1)
                if not has_begin_string[0] == - 1:
                    beginning_bibliography = True

            if beginning_bibliography: 
                if r"%\cite{" in file_line:
                    pass
                elif r'\bibitem{' in file_line:
                    itag = find_first_substring_tag(file_line, ini_tag = r'\bibitem{', end_tag = '}')
                    if itag[1] == -1:
                        raise SyntaxError("Unclose \bibitem{ !")
                    if is_first_bib:
                        line = file_line

                    if not is_first_bib:
                        output_dict = {**output_dict, **AddBibliography(current_tag, line, incitation_standard, outcitation_standard)}
                        del line
                        line = file_line

                    current_tag = file_line[itag[0] + len(r'\bibitem{'): itag[1]]

                    is_first_bib = False
                else:
                    line = line + file_line

        if not current_tag == '':
            output_dict = {**output_dict, **AddBibliography(current_tag, line, incitation_standard, outcitation_standard)}


    return output_dict

def export_orderedtags_to_file(list_tags: list, list_bib: list, outfilename: str = 'exported_tags.txt')-> list:
    '''
      Get a list of citation tags and a list of bib itens and export to file ordered and in a neat format. 
      Return a list of unused tags
    '''

    unused_tags = []

    for tag in list_bib:
        if tag not in list_tags:
            unused_tags.append(tag)

    ## record the unused tags.
    if len(unused_tags) > 0 : 
        with open(outfilename[:-4]+'_not_used.txt','w+') as fp:
            for tag in unused_tags:
                fp.write(str(list_bib[tag]))
                fp.write("\n\n")

    with open(outfilename,'w+') as fp:
        tagnumb = 0
        for tag in list_tags:
            if tag not in list_bib:
                fp.write(r'{\color{red}\n\t\bibitem{' + tag + '}\n')
                fp.write('\t\t Reference not found!!\n' + r'%%%%%% reference not found in bibliography! %%%%%%'+'\n}'+'\n\n')
            else:
                tagnumb = tagnumb + 1
                if list_bib[tag].GetOutPutFormat() == "markdown":
                    fp.write(f"[{tagnumb}] ")
                fp.write(str(list_bib[tag]))
                fp.write("\n\n")

    return unused_tags