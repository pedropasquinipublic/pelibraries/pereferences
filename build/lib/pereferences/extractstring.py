#########################################################
###                 created by                          ### 
###                Pedro Pasquini                          ###
###        https://inspirehep.net/authors/1467863          ###
###                                                   ### 
###                 creation date                      ### 
###                 20st Aug 2020                      ### 
#########################################################

import numpy as np


def find_first_substring_tag(s: str, ini_tag : str, end_tag , add_ini_tag_len = False) -> list:
    '''
      Function to find the index location of the first substring inside of a given tag.
    '''
    Nst0 = s.find(ini_tag)
    if Nst0 >=0:

        if add_ini_tag_len:
            naux = len(ini_tag)
        else: 
            naux = 0

        if end_tag == - 1:
            return [Nst0  + naux, len(s) - 1]
        
        Nst1 = s[Nst0:].find(end_tag)

        if Nst1 == - 1:
            return [Nst0  + naux, -1]

        return [Nst0  + naux, Nst0 + Nst1]

    return [-1,-1]


def find_all_substrings(s:str, ini_tag : str, end_tag)-> list:
    '''
      Function to find the index of all substrings inside a tag and return them as a string list ordered.
    '''

    s = s.replace(" ","")
    locations = find_first_substring_tag(s, ini_tag = ini_tag, end_tag = end_tag)

    if locations[0] == -1:
        return []

    loc_list = [locations]


    aux_string = s[locations[1]:]
    while not locations[0] == -1 or locations[1] >= len(aux_string):
        locations = find_first_substring_tag(aux_string, ini_tag = ini_tag, end_tag = end_tag)
        if not locations[0] < 0:
            aux_loc = loc_list[len(loc_list)-1]
            loc_list.append([aux_loc[1]+locations[0],aux_loc[1]+locations[1]])
            aux_string = aux_string[locations[1]:]

    return [s[loc[0] + len(ini_tag):loc[1]- 1] for loc in loc_list]


def CheckFoundFirstTagOnly(tag_list: list, inString: str, ini_tag : str, end_tag, string_separator = -1)-> tuple((bool, int)):
    ''' 
      Check if only the first tag appears. If it does not, add to the tag list. otherwise return bool to keep the string and the position of the first tag
    '''
    aux = find_first_substring_tag(inString, ini_tag = ini_tag, end_tag = end_tag)

    ## if we find the start and end of the tags, we keep the content inside.
    if aux[0] > -1 and aux[1] > -1:
        if string_separator == -1:
            tag_list.append(inString[aux[0] + len(ini_tag):aux[1]])
        else:
            aux_string = inString[aux[0] + len(ini_tag):aux[1]].split(string_separator)
            tag_list.extend(aux_string)

        return False, -1
    ## if we only find the first tag, we keep the line, since the end tag might be in the next lines.
    elif aux[0] > -1 and aux[1] == -1:
        return True, aux[0]
    
    return False, -1

def RemovePatternFromString(instring:str, iniPattern:str, endPattern: str) -> str:
    '''
        Check if a string has some pattern iniPattern ... endPattern and remove it.
    '''
    
    i_aux = find_first_substring_tag(instring, ini_tag = iniPattern, end_tag = endPattern)

    if i_aux[0] > -1:
        outputstring   = instring[: i_aux[0]] 
        outputstring   = outputstring + instring[i_aux[0] + len(iniPattern): i_aux[1]]
        return outputstring + instring[i_aux[1] + len(endPattern) : ]
    else:
        return instring


def extract_strings_from_file(file_path: str, ini_tag: str, end_tag: str, stop_string_ini: str = r'\end{document', stop_string_end: str = '}', string_separator = ",", comment_string : str = r"%", max_line_size = 30000)-> list:
    '''
      Given a file path, find the list of strings inside a patern and stops at the end of file or when finding a given pattern.
    '''
    extracted_tags = []

    with open(file_path) as fp:
        reached_end         = False
        keep_previous_line  = False
        line = ""

        for file_line in fp:

            ## check if found end string of file.
            has_end_string = find_first_substring_tag(file_line, ini_tag = stop_string_ini, end_tag = stop_string_end)
            if not has_end_string[0] == - 1:
                reached_end = True


            partial_line = file_line.replace(" ","")

            ## first we remove comments
            icomment, _ = find_first_substring_tag(partial_line, ini_tag = comment_string, end_tag = -1)
            if icomment > -1:
                if icomment == 0:
                    partial_line = partial_line[: icomment]
                if icomment > 0:
                    if not partial_line[icomment - 1 : icomment + 1] == r'\%':
                        partial_line = partial_line[: icomment]

            ## Now we need to see if there is the initial tag in file.
            ## Notice that it might happen that the initial tag is found, but the final is not, 
            ## since it might be in next line. So we merge several lines until the final tag is found.

            ### first we chek if we dont need to keep previous line
            if not keep_previous_line:
                keep_previous_line, ifirstag = CheckFoundFirstTagOnly(extracted_tags, partial_line,  ini_tag = ini_tag, end_tag = end_tag, string_separator = string_separator)

                if keep_previous_line:
                    del line 
                    line = partial_line[ifirstag:- len("\n")]

            else:
                ## if we keep the previous line, we need to check if we can find the end tag in it.
                ifound, _  = find_first_substring_tag(partial_line, ini_tag = end_tag, end_tag = -1)
                ## if we still didn't find the final tag. If that is the case, we simply merge lines.
                if ifound > -1:

                    #print(line[len(ini_tag):] + partial_line[: ifound])
                    ### We found the end tag!.
                    if string_separator == -1:
                        extracted_tags.append(line[len(ini_tag):] + partial_line[: ifound])
                    else:
                        aux_string = (line[len(ini_tag):] + partial_line[: ifound]).split(string_separator)
                        extracted_tags.extend(aux_string)

                    del line 
                    line = ''
                    keep_previous_line = False

                    ## It might happen that the end of the string also contains a new tag. In that case we need to check for that.
                    keep_previous_line, ifirstag = CheckFoundFirstTagOnly(extracted_tags, partial_line[ifound:],  ini_tag = ini_tag, end_tag = end_tag, string_separator = string_separator)
                    if keep_previous_line:
                        del line 
                        line = partial_line[ifirstag: - len("\n")]
                else: 
                    line = line + partial_line[ifirstag: - len("\n")]
                        

            if len(line) > max_line_size:
                raise BufferError("Buffer string got too long! Maybe you forgot to close yout end tag?")

            if reached_end:
                break
    
    uniq, index = np.unique(extracted_tags, return_index=True)

    return uniq[index.argsort()]

def export_tags_to_file(list_tags: list, filename: str = 'exported_bib.txt')-> None:
    '''
      Given a list of tags, export the tags into a file
    '''
    with open(filename,'w+') as fp:
        for tag in list_tags:
            fp.write(tag)
            fp.write("\n")