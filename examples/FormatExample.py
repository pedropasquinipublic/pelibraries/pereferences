
from pereferences.citation import Citation

if __name__ == "__main__":
	
	test_string = " %\cite{Fornengo:2001pm}\n"
	test_string = test_string + "\\bibitem{Fornengo:2001pm}\n"
	test_string = test_string + "N.~Fornengo, M.~Maltoni, R.~Tomas and J.~W.~F.~Valle,\n"
	test_string = test_string + "%``Probing neutrino nonstandard interactions with atmospheric neutrino data,''\n"
	test_string = test_string + "Phys. Rev. D \\textbf{65} (2002), 013010\n"
	test_string = test_string + "doi:10.1103/PhysRevD.65.013010\n"
	test_string = test_string + "[arXiv:hep-ph/0108043 [hep-ph]].\n"


	myCitation = Citation(test_string)

	print(myCitation)


