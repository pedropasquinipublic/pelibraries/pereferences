
from pereferences.orderbib import export_orderedtags_to_file, ExtractBibliographyInfo

if __name__ == "__main__":
	
    bib_list = ExtractBibliographyInfo('./INSPIRE-PedroPasquini.tex', 
                                       outcitation_standard = "markdown", 
                                       startFromFileBeginning = True)


    export_orderedtags_to_file(bib_list.keys(), bib_list, outfilename = "MarkdownBib.md")
