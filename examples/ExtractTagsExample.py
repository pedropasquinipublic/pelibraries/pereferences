
from pereferences.extractstring import extract_strings_from_file
from pereferences.orderbib import export_orderedtags_to_file, ExtractBibliographyInfo

if __name__ == "__main__":
	

	## use 'LaTeX (US)' if you want to preserve LaTeX US
	##bib_list = ExtractBibliographyInfo('./test_document.tex', outcitation_standard = 'LaTeX (US)') #
	bib_list = ExtractBibliographyInfo('./test_document.tex')



	## first we collect the references
	print()
	print("Bibliography refs")
	print("------------------------------")
	print(bib_list.keys())
	print("------------------------------")
	print(f'Unique bibliography tags found: {len(bib_list.keys())}')

	
	## now the citation tags
	ordered_tag = extract_strings_from_file('./test_document.tex', ini_tag = r"\cite{", end_tag = '}')
	print("Cited tags")
	print("------------------------------")
	print(ordered_tag)
	print("------------------------------")
	print(f'Unique citation tags found: {len(ordered_tag)}')
	
	with open("./cited_tags.tex", "w+") as fp:
		fp.write(r"\begin{document}"+"\n\n")
		fp.write("Use this file as input for InspireHEP tools bibliography generator!\n")
		for tag in ordered_tag:
			fp.write(r"\cite{"+tag+"}")
			fp.write("\n")

		fp.write(r"\end{document}"+"\n\n")

	unused_tags = export_orderedtags_to_file(ordered_tag, bib_list)


	print()
	print(f'Unused tags found: {len(unused_tags)}')


