from setuptools import setup


setup(
    name='pereferences',
    version='1.0.1',    
    description='Some usefull functions for managing references on a manuscript',
    url='https://gitlab.com/pedropasquinipublic/pelibraries/pereferences',
    author='Pedro Pasquini',
    author_email='pedrosimpas@gmail.com',
    packages=['pereferences'],
    install_requires=[
                      'numpy'                   
                      ],

    classifiers=[
        'Intended Audience :: Science/Research', 
        'Operating System :: POSIX :: Linux',      
        'Programming Language :: Python :: 3.8',
    ],
)
