________________________________________________
<div align="center">
peReferences

A python library for organizing and styling your refereces

Creation date: 20 August 2020          
Created by 

Pedro Pasquini      
<https:/inspirehep.net/authors/1467863>    
</div>


________________________________________________

# Introduction
This file contains some useful functions and classes to organize and stylize references on a manuscript in LaTeX format.

# Instalation

To **install** you can use pip. 

Go to folder in terminal and run 
>> pip3 install -e ../pereferences

To **uninstall** it just type
>> pip3 uninstall pereferences


# Interesting Functions

The two most important functions are 

1. Function to extract the bibliography information from a .tex file. 
The bibliography should start with \begin{bibliography} and end with \end{bibliography}. It retunrs a dictionary with all the citations as the class 
pereferences.citation.Citation(),

>> ExtractBibliographyInfo(inputfilename: str) -> dict(Citation)

2. Function to extract possible a collection of words between a given pattern
of ini_tag and end_tag. Return a list with all tags found, with no repetition and 
in the order they appear.

>> extract_strings_from_file(inputfilename: str, ini_tag : str, end_tag) -> list

See usage examples in the example files

# Bibliography Standards

Current input bibliography standards are:

1. Latex (US)

Current output bibliography standards are:

1. My neat standard
2. Using the same as input


I will add more in the future.


