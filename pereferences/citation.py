#########################################################
###                                 created by                                                  ### 
###                                Pedro Pasquini                                                  ###
###                https://inspirehep.net/authors/1467863                  ###
###                                                                                                   ### 
###                                 creation date                                          ### 
###                                 20st Aug 2020                                          ### 
#########################################################

from datetime import date
from pereferences.extractstring import find_first_substring_tag, RemovePatternFromString

peCitationStandards = ['LaTeX (US)', 'neat', 'keep', 'markdown']


class arXivCitation:
        number  : str 
        subject : str
        def __init__(self, number, subject) -> None:
                self.number   = number
                self.subject  = subject

class JournalCitation:
        reference : str 
        doi       : str
        Erratum   : str
        def __init__(self, reference, doi, Erratum = False) -> None:
                self.reference = reference
                self.doi       = doi
                self.Erratum   = Erratum

class Citation:
    title            : str 
    authorlist       : dict
    tag              : str
    referencelist    : dict
    citations        : str
    releasedate      : date
    is_published     : bool 
    outputformat     : str
    OriginalCitation : str

    def __init__(self, inCitation, incitation_standard: str = 'LaTeX (US)',  outputformat: str = 'neat') -> None:
        if not outputformat in peCitationStandards:
            raise NotImplementedError(f"The {outputformat} citation format is not implemented yet!")

        self.title = None
        self.authorlist = {}
        self.tag = None
        self.referencelist = {}
        self.citations = None
        self.releasedate = None
        self.is_published = False
        self.Erratum = False

        self.OriginalCitation = inCitation

        self.outputformat = outputformat

        self.ExtractInfo(inCitation, incitation_standard)

    def NeatFormatting(self)-> str:
        return_string = r'\bibitem{'+self.tag+'}\n'
        return_string = return_string + '   '+ self.authorlist +'\n'
        return_string = return_string + r' ``\textit{' + self.title + "},''\n"
        if "Journal" in self.referencelist:
                return_string = return_string + r' \href{http://dx.doi.org/' + self.referencelist["Journal"].doi + '}\n      {'+ self.referencelist["Journal"].reference + '}\n'
        if "arXiv" in self.referencelist:
                return_string = return_string + r' [\href{http://arxiv.org/abs/' + self.referencelist["arXiv"].number + '}\n      {arXiv:'+ self.referencelist["arXiv"].number + '} [' + self.referencelist["arXiv"].subject + ']]\n'

        return_string = return_string[:-1] + '.\n\n'
        
        return return_string.rstrip()               

    def MarkdownFormatting(self)-> str:

        return_string = self.authorlist.replace("~", " ") +'    \n'
        return_string = return_string + "''<em>" + self.title + "</em>'',    \n"
        if "Journal" in self.referencelist:
            return_string = return_string + '<a href="http://dx.doi.org/' + self.referencelist["Journal"].doi + '">'

            auxJournalName = RemovePatternFromString(self.referencelist["Journal"].reference, r"\textbf{", "}")
            return_string = return_string + auxJournalName + "</a>    \n"

            if self.referencelist["Journal"].Erratum:
                auxErratum = RemovePatternFromString(self.referencelist["Journal"].Erratum, r"\textbf{", "}")
                return_string = return_string + auxErratum +"   \n"

        if "arXiv" in self.referencelist:
            return_string = return_string + '[<a href="http://arxiv.org/abs/' + self.referencelist["arXiv"].number + '"> arXiv:'
            return_string = return_string + self.referencelist["arXiv"].number + "</a> [hep-ph]]    \n"

        return_string = return_string[:-5] + '.    \n\n'

        return return_string.rstrip() 

    def __str__(self) -> str:
        if self.outputformat == 'neat':
            return self.NeatFormatting()
        if self.outputformat == 'markdown':
            return self.MarkdownFormatting()
        if self.outputformat == 'keep':
            return self.OriginalCitation.rstrip()

        raise NotImplementedError(f"The {self.outputformat} citation format is not implemented yet!")

    def ExtractInfo(self, inCitation:str, citation_standard: str = 'LaTeX (US)') -> None:
        if citation_standard == 'LaTeX (US)':
            self.ExtractLatexUSStandard(inCitation)
        else: 
            raise NotImplementedError(f"The {citation_standard} citation standard is not implemented yet!")
    
    def ExtractLatexUSStandard(self, inCitation: str) -> None:

            i_aux = find_first_substring_tag(inCitation, ini_tag = r'\bibitem{', end_tag = '}')
            print(i_aux)
            if i_aux[0] > -1:
                    self.tag = inCitation[i_aux[0] + len(r'\bibitem{'): i_aux[1]]
            else:
                    self.tag = ""

            i_aux = find_first_substring_tag(inCitation, ini_tag = "%``", end_tag = ",''")
            if i_aux[0] > -1:
                    self.title = inCitation[i_aux[0] + len("%``"): i_aux[1]]
            else:
                    self.title = ""

            doiParser_ini = "doi:"
            i_aux = find_first_substring_tag(inCitation, ini_tag = doiParser_ini, end_tag = "\n")
            if i_aux[0] > -1:
                    self.is_published = True
                    Erratum = False
                    Journal_doi  = inCitation[i_aux[0] + len(doiParser_ini): i_aux[1]]
                    aux_line = ''
                    for line in inCitation.split("\n"):
                            if "[erratum:" in line:
                                Erratum = line
                                JournalReference = aux_line
                                break
                            elif "doi:" in line:
                                JournalReference = aux_line
                                break
                            aux_line = line 
                    
                    self.referencelist = {"Journal" : JournalCitation(JournalReference, Journal_doi, Erratum)}
            else:
                    self.is_published = True

            arxivParser_ini = "[arXiv:"
            i_aux = find_first_substring_tag(inCitation, ini_tag = arxivParser_ini, end_tag = " [")
            if i_aux[0] > -1:
                    arxiv_number = inCitation[i_aux[0]+len(arxivParser_ini): i_aux[1]]
                    aux_string   = inCitation[i_aux[0]:]
                    Arxiv_subject   = find_first_substring_tag(aux_string, ini_tag = arxiv_number+" [", end_tag = "]]")
                    Arxiv_subject   = aux_string[Arxiv_subject[0] + len(arxiv_number) + 2: Arxiv_subject[1]]
                    self.referencelist = {**self.referencelist, **{"arXiv" : arXivCitation(arxiv_number, Arxiv_subject)}}

            citationParser_ini = ".\n"+r'%'
            i_aux = find_first_substring_tag(inCitation, ini_tag = citationParser_ini, end_tag = 'citations')
            if i_aux[0] > -1:
                    self.citations = inCitation[i_aux[0] + len(citationParser_ini): i_aux[1]].replace(" ","")
            else:
                    self.citations = ""

            i_aux = find_first_substring_tag(inCitation, ini_tag = r'\bibitem{'+self.tag+"}\n", end_tag = "\n%``"+self.title+",''")
            ## For now I am just using a string for author list, which is simpler to store. In the future I will separate each author.
            if i_aux[0] > -1:
                    self.authorlist = inCitation[i_aux[0] + len(r'\bibitem{'+self.tag+"}\n"): i_aux[1]]
            else:
                    self.authorlist = ""

    def GetCitationNumber(self) -> str:
        return self.citations

    def GetPublishedStatus(self) -> bool:
        return self.is_published

    def GetReferenceList(self) -> dict:
        return self.referencelist
    
    def GetOutPutFormat(self) -> str:
          return self.outputformat